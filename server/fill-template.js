import JSZip from 'jszip';
import Docxtemplater from 'docxtemplater';

import fs from 'fs';
import path from 'path';

const getDocContent = (data) => {
  //Load the docx file as a binary
  const content = fs.readFileSync('./template.docx', 'binary');

  const zip = new JSZip(content);

  const doc = new Docxtemplater();
  doc.loadZip(zip);

  //set the templateVariables
  doc.setData(data);

  try {
    // render the document (replace all occurences of {first_name} by John, {last_name} by Doe, ...)
    doc.render()
  }
  catch (error) {
    const e = {
      message: error.message,
      name: error.name,
      stack: error.stack,
      properties: error.properties,
    }
    console.log(JSON.stringify({ error: e }));
    // The error thrown here contains additional information when logged with JSON.stringify (it contains a property object).
    throw error;
  }
  return doc.getZip().generate({type: 'base64'});
};

const writeContent = (buf) => {
  fs.writeFileSync(path.resolve(__dirname, 'output.docx'), buf);
};

module.exports = {
  getDocContent,
  writeContent
} 