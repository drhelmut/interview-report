import { getDocContent, writeContent } from './fill-template';

describe('fill-template', () => {
  it('Should generate file from template', () => {
    const content = getDocContent({
      consultant_name: 'Hugo Capocci',
      interview_date: '17/07/2018',
      client_company_name: 'Talent Soft',
      client_attendees: [
        { name: 'Pierre-Atoine Schaeffer', role: 'Product Owner' }
      ],
      project_name: '',
      technical_environment: [
        { description: 'Java 8, TDD, BDD' }
      ],
      rating: 5,
      pros: 'Gros challenge technique',
      cons: 'Très loin de chez moi (1h30 de transport)',
      resume: '',
      mission_description: '',
      interview_report: ''
    });
    // expect(content).toMatchSnapshot();
    writeContent(content);
  });

});
