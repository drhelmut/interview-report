var path = require('path');
var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin'); 

module.exports = {
  entry: path.join(__dirname, './views/index.js'),
  output: {
    path: path.join(__dirname, './build'),
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        query: {
          presets: ["env", "stage-0", "react",  "flow", "mobx"],
          plugins: []
        }
      },
      {
        test: /\.css/,
        use: ['style-loader', 'css-loader'],
      }
    ]
  },
  stats: {
    colors: true
  },
  devtool: 'source-map',
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, './public/index.html'),
      filename: 'index.html',
      inject: 'body',
    }),
  ],
};