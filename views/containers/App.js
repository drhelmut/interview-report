// @flow
import React from 'react';
import IntroText from '../components/IntroText';
import { Tabs, Tab } from 'react-bootstrap';
import LocaleProvider, {LocaleContext } from  '../components/LocaleProvider';
import './App.css';

export default function App() {
  return <LocaleProvider>
    <LocaleContext.Consumer>
    { ({ report }) => {
      return (
      <Tabs defaultActiveKey={1} animation={true}>
        <Tab eventKey={1} title="Page 1">
          <IntroText report={report}/>
        </Tab>
        <Tab eventKey={2} title="Page 2">
          Page 2
        </Tab>
      </Tabs>
    )}}
    </LocaleContext.Consumer>
  </LocaleProvider>
  ;
};
