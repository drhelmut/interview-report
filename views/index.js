import React from 'react';
import { render } from 'react-dom';
// import { observe } from "mobx";

import App from './containers/App';

// import Todo from './Todo';

render(<App />, document.getElementById('root'));

// const todo = new Todo();

// observe(todo, (values) => {
//   console.log('title has changed: ', values);
// });

// todo.setTitle('title 1');
// todo.setTitle('title 2');

// todo.title = 'title no strict';
