// @flow
/* global expect, jest */

import React from 'react';
import { shallow } from 'enzyme';
import moxios from 'moxios';

import IntroText from './IntroText';
import Report from '../../stores/Report';

const report = new Report();

describe('tests for <IntroText> component', () => {
  beforeEach(() => {
    moxios.install();
  });

  afterEach(() => {
    moxios.uninstall();
  });

  it('should render', () => {
    const component = shallow(<IntroText report={report} />);
    expect(component.find('div').length).not.toEqual(0);
  });

  it('should send data on click', (done) => {
    moxios.stubRequest('/download', {
      status: 200,
      response: 'hello'
    });

    const component = shallow(<IntroText report={report} />);

    component.instance().onClick();
    moxios.wait(() => {
      expect(component.state('downloadData')).toEqual('hello');
      done();
    });
  });

  it('should change report values', () => {
    const component = shallow(<IntroText report={report} />);

    component.instance().onChange('fakeValue', 'consultantName');
    expect(report.consultantName).toEqual('fakeValue');

    component.instance().onChange('fakeRole', 'role', 'clientAttendees', 0);
    expect(report.clientAttendees).toEqual([{
      name: '',
      role: 'fakeRole'
    }]);
  });
});
