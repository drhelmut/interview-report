// @flow
/* global expect, jest */

import React from 'react';
import { shallow } from 'enzyme';
import InputText from './InputText';

const props = {
  name: 'inputName',
  value: 'inputValue',
  onChange: jest.fn()
}
describe('tests for <InputText> component', () => {
  it('should call props.onChange when value is changed', () => {
    const component = shallow(<InputText {...props} />);
    expect(component.find('input').length).toEqual(1);

    component.simulate('change', { target: { value: 'newInputValue' } });
    expect(props.onChange).toBeCalledWith('newInputValue');
  });
});
