import React from 'react';

export const LocaleContext = React.createContext();

import Report from '../../stores/Report';

const report = new Report();

class LocaleProvider extends React.Component {
  render() {
    return (
      <LocaleContext.Provider value={{ report }}>
        {this.props.children}
      </LocaleContext.Provider>
    );
  }
}

export default LocaleProvider;
