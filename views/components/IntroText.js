// @flow
import React from 'react';
import axios from 'axios';
import DatePicker from 'react-date-picker';
import { format } from 'date-fns';
import DownloadButton from 'react-dfb';
import type { DownloadData } from 'react-dfb';
import { observer } from 'mobx-react';
import Rating from 'react-rating';

import InputText from './InputText';

type Props = {
  report: any;
};

type State = {
  downloadData: DownloadData,
};

type ClientAttendee = {
  name: string,
  role: string
};

@observer
export default class IntroText extends React.Component<Props, State> {
  state = {
    downloadData: {
    },
  };

  onChange = (value: any, attributeName: string, collection?: string, index?: number) => {
    if (collection !== undefined && index != null) {
      this.props.report.setValueInCollection(collection, index, attributeName, value);
      return;
    }
    this.props.report.setValue(attributeName, value);
  };

  onClick = () => {
    const data = {
      consultant_name: this.props.report.consultantName,
      interview_date: format(this.props.report.interviewDate, 'DD/MM/YYYY'),
      client_company_name: this.props.report.clientCompanyName,
      client_attendees: this.props.report.clientAttendees,
      project_name: 'Pas de nom spécifique',
      technical_environment: [
        { description: 'Java 8, TDD, BDD' }
      ],
      rating: this.props.report.rating,
      pros: 'Gros challenge technique',
      cons: 'Très loin de chez moi (1h30 de transport)',
      resume: '',
      mission_description: '',
      interview_report: ''
    };

    axios.post('/download', data)
    .then((response) => {
      this.setState({
        downloadData: response.data
      });
    })
    .catch(function (error) {
      console.log(error);
    });
  };

  onRateChange = (value: number) => {
    if (value == null) return;
    console.log('rate change to ', value);
    this.props.report.setValue('rating', value);
  }

  render() {
    return (<div>
      <label htmlFor='consultantName'>Nom du consultant </label>
      <InputText
        name='consultantName'
        value={this.props.report.consultantName}
        onChange={(value: string) => this.onChange(value, 'consultantName')}
      />
      <label htmlFor='interviewDate'>Date de l'entretien </label>
      <DatePicker
        value={this.props.report.interviewDate}
        name='interviewDate'
        onChange={(value) => this.onChange(value, 'interviewDate')}
      />

      <label htmlFor='clientCompanyName'>Nom du client (société) </label>
      <InputText
        name='clientCompanyName'
        value={this.props.report.clientCompanyName}
        onChange={(value: string) => this.onChange(value, 'clientCompanyName')}
      />
      <span>Nom(s) du(es) interlocuteurs de l’entretien </span>
      { this.props.report.clientAttendees.map((clientAttendee, index) => (
        <div className="row" key={index}>
          <label htmlFor={'clientAttendeeName' + index}>Nom </label>
          <InputText
            name={'clientAttendeeName' + index}
            value={clientAttendee.name}
            onChange={(value: string) => this.onChange(value, 'name', 'clientAttendees', index)}
          />

          <label htmlFor={'clientAttendeeRole' + index}>Role </label>
          <InputText
            name={'clientAttendeeRole' + index}
            value={clientAttendee.role}
            onChange={(value: string) => this.onChange(value, 'role', 'clientAttendees', index)}
          />

          <button type="button" className="btn btn-default btn-sm" onClick={() => this.props.report.addEmptyItemInCollection('clientAttendees')}>
            <span className="glyphicon glyphicon-plus"></span>
          </button>

          { index > 0 &&
            <button type="button" className="btn btn-default btn-sm" onClick={() => this.props.report.removeItemInCollection('clientAttendees', index)}>
              <span className="glyphicon glyphicon-minus"></span>
            </button>
          }
        </div>
      ))}
      <Rating
        fractions={2}
        onChange={this.onRateChange}
        onHover={this.onRateChange}
        initialRating={this.props.report.rating}
        fullSymbol={<span className="glyphicon glyphicon-star icon" style={{ color: 'goldenrod' }} ></span>}
        emptySymbol={<span className="glyphicon glyphicon-star-empty icon" style={{ color: 'orange' }} ></span>}
      />
      <DownloadButton onClick={this.onClick} downloadData={this.state.downloadData} />
    </div>);
  }
};
