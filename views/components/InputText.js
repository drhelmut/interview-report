// @flow
import React from 'react';

type Props = {
  name: string,
  value: string,
  onChange: Function,
};

export default class InputText extends React.PureComponent<Props> {
  render() {
    return (
      <input
        type='text'
        name={this.props.name}
        value={this.props.value}
        onChange={({target}: {target: HTMLInputElement}) => this.props.onChange(target.value)}
      />
    );
  }
};
