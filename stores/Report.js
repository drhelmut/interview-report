// @flow
import { observable, action } from 'mobx';

type ClientAttendee = {
  name: string,
  role: string
};

const emptyItems = {
  clientAttendees: {
    name: '',
    role: ''
  }
};

export default class Report {
  @observable consultantName: string = '';
  @observable clientCompanyName: string = '';
  @observable interviewDate: Date = new Date();
  @observable clientAttendees: Array<ClientAttendee> = [ emptyItems['clientAttendees'] ];
  @observable rating: number = 0;

  @action setValue(attributeName: string, value: string|number) {
    // $FlowFixMe
    if (this[attributeName] == null) return;
    // $FlowFixMe
    this[attributeName] = value;
  }

  @action setValueInCollection(collection: string, index: number, attributeName: string, value: string) {
    // $FlowFixMe
    if (!this[collection] || !this[collection][index]) return;
    this[collection][index][attributeName] = value;
  }

  @action addEmptyItemInCollection(collection: string) {
    // $FlowFixMe
    if (!this[collection]) return;
    this[collection].push(emptyItems[collection]);
  }

  @action removeItemInCollection(collection: string, index: number) {
    // $FlowFixMe
    if (!this[collection]) return;
    this[collection].splice(index, 1);
  }
}
