// @flow
/* global expect, jest */

import Report from './Report';

let report;

describe('tests for <Report> store', () => {
  beforeEach(() => {
    report = new Report();
  });

  it('Should set attribute value',() => {
    expect(report.clientCompanyName).toEqual('');
    report.setValue('clientCompanyName', 'fakeCompany');
    expect(report.clientCompanyName).toEqual('fakeCompany');
  });

  it('should add empty item from collection', () => {
    report.addEmptyItemInCollection('clientAttendees');
    expect(report.clientAttendees.length).toEqual(2);
  });

  it('should remove item from collection', () => {
    expect(report.clientAttendees).toEqual([
      {
        name: '',
        role: ''
      }
    ]);
    report.removeItemInCollection('clientAttendees', 0);
    expect(report.clientAttendees).toEqual([]);
  });

  it('should modify item attribute value,in collection', () => {
    report.setValueInCollection('clientAttendees', 0, 'role', 'fakeRole');
    expect(report.clientAttendees).toEqual([
      {
        name: '',
        role: 'fakeRole'
      }
    ]);
  });
});
